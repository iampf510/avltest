from django.contrib import admin

from .models import Restaurant, ResType
# Register your models here.

@admin.register(ResType)
class ResTypeAdmin(admin.ModelAdmin):
    list_display = ('name', )

@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
    list_display = ('name', 
            'mon_start', 'mon_end')
    #    mon_start = models.CharField()
    #mon_end = models.CharField(max_length=30)
    #tue_start = models.CharField(max_length=30)
    #tue_end = models.CharField(max_length=30)
    #wed_start = models.CharField(max_length=30)
    #wed_end = models.CharField(max_length=30)
    #thu_start = models.CharField(max_length=30)
    #thu_end = models.CharField(max_length=30)
    #fri_start = models.CharField(max_length=30)
    #fri_end = models.CharField(max_length=30)
    #sat_start = models.CharField(max_length=30)
    #sat_end = models.CharField(max_length=30)
    #sun_start = models.CharField(max_length=30)
    #sun_end = models.CharField(max_length=30)


    #restype = models.ForeignKey(
    #    ResType,
    #    related_name='restaurants',
    #    on_delete=models.DO_NOTHING)


    #parking = models.BooleanField(default=False)
    #mick = models.models.IntegerField()
    #delivery = models.BooleanField(default=False)
    #deposit = models.BooleanField(default=False)
    #rating = models.IntegerField()
    #gps_x = models.DecimalField(max_digits=3, decimal_places=10)
    #gps_y = models.DecimalField(max_digits=3, decimal_places=10)
