from django.db import models

# Create your models here.

class ResType(models.Model):
    name = models.CharField(max_length=30,null=True,blank=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

class Restaurant(models.Model):
    name = models.CharField(max_length=30,null=True,blank=True)


    mon_start = models.CharField(max_length=30,null=True,blank=True)
    mon_end = models.CharField(max_length=30,null=True,blank=True)
    tue_start = models.CharField(max_length=30,null=True,blank=True)
    tue_end = models.CharField(max_length=30,null=True,blank=True)
    wed_start = models.CharField(max_length=30,null=True,blank=True)
    wed_end = models.CharField(max_length=30,null=True,blank=True)
    thu_start = models.CharField(max_length=30,null=True,blank=True)
    thu_end = models.CharField(max_length=30,null=True,blank=True)
    fri_start = models.CharField(max_length=30,null=True,blank=True)
    fri_end = models.CharField(max_length=30,null=True,blank=True)
    sat_start = models.CharField(max_length=30,null=True,blank=True)
    sat_end = models.CharField(max_length=30,null=True,blank=True)
    sun_start = models.CharField(max_length=30,null=True,blank=True)
    sun_end = models.CharField(max_length=30,null=True,blank=True)


    restype = models.ForeignKey(
        ResType,
        related_name='restaurants',
        on_delete=models.DO_NOTHING)


    parking = models.BooleanField(default=False)
    mick = models.IntegerField(default=0)
    delivery = models.BooleanField(default=False)
    deposit = models.BooleanField(default=False)
    rating = models.DecimalField(max_digits=3, decimal_places=2)
    gps_x = models.DecimalField(max_digits=15, decimal_places=10)
    gps_y = models.DecimalField(max_digits=15, decimal_places=10)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

#class Booking(models.Model):
#    user = 



